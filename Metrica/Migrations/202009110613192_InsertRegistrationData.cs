namespace Metrica.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class InsertRegistrationData : DbMigration
    {
        public override void Up()
        {
            Sql("INSERT INTO Registrations(Name, PresentAddress, PermanentAddress, Email, PhoneNo, SkypeId, Role) VALUES('Mahabub', 'Mirpur', 'Gaibandha', 'mahabub13d1995@gmail.com', '01681416002', 'pasha13d', 'Admin')");
            Sql("INSERT INTO Registrations(Name, PresentAddress, PermanentAddress, Email, PhoneNo, SkypeId, Role) VALUES('Rajib', 'Mirpur', 'Chandpur', 'rajibGit@gmail.com', '01681414578', 'rajibSkype', 'User')");
        }
        
        public override void Down()
        {
        }
    }
}
