﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Metrica.Models
{
    public class Login
    {
        [Display(Name = "User Name")]
        public string UserName { get; set; }

        public string Password { get; set; }
    }
}