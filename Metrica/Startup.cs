﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Metrica.Startup))]
namespace Metrica
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
