﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Metrica.Models;

namespace Metrica.ViewModel
{
    public class RegistrationFormViewModel
    {
        public Registration Registration { get; set; }
    }
}