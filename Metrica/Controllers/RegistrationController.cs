﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Metrica.Models;
using Metrica.ViewModel;

namespace Metrica.Controllers
{
    public class RegistrationController : Controller
    {
        private ApplicationDbContext _context;

        public RegistrationController()
        {
            _context = new ApplicationDbContext();
        }

        protected override void Dispose(bool disposing)
        {
            _context.Dispose();
        }

        public ActionResult NewRegistration()
        {
            //var registration = _context.Registrations.ToList();
            var viewModel = new RegistrationFormViewModel()
            {
                Registration = new Registration()
            };
            return View("RegistrationForm", viewModel);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Save(Registration registration)
        {
            if (!ModelState.IsValid)
            {
                var viewModel = new RegistrationFormViewModel
                {
                    Registration = registration
                };
                return View("RegistrationForm", viewModel);
            }

            if (registration.Id == 0)
                _context.Registrations.Add(registration);
            else
            {
                var registrationInDb = _context.Registrations.Single(r => r.Id == registration.Id);
                
                registrationInDb.Name = registration.Name;
                registrationInDb.PresentAddress = registration.PresentAddress;
                registrationInDb.PermanentAddress = registration.PermanentAddress;
                registrationInDb.Email = registration.Email;
                registrationInDb.SkypeId = registration.SkypeId;
                registrationInDb.PhoneNo = registration.PhoneNo;
                registrationInDb.Role = registration.Role;
            }
            _context.SaveChanges();
            return RedirectToAction("Index", "Registration");
        }

        // GET: Registration
        public ActionResult Index()
        {
            var registration = _context.Registrations.ToList();
            return View(registration);
        }

        public ActionResult Edit(int id)
        {
            var registration = _context.Registrations.SingleOrDefault(c => c.Id == id);
            if (registration == null)
                return HttpNotFound();
            var viewModel = new RegistrationFormViewModel
            {
                Registration = registration
            };
            return View("RegistrationForm", viewModel);
        }
    }
}